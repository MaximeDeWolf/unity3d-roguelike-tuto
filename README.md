# Unity3D-RogueLike-Tuto

This game has been made by following this tutorial: https://unity3d.com/fr/learn/tutorials/s/2d-roguelike-tutorial.
We used the *2018.3.9f1 Unity version* to make it. The assets pack we used is totally and can be download here:
 https://assetstore.unity.com/packages/essentials/tutorial-projects/2d-roguelike-29825. This game is a pretty simple
 _roguelike_ where the player controlls a survivor who have to collect food and soda by avoiding some zombies in
order to survive for as long as possible.

## Features

Here is all the features we learn to make with *Unity* during _this_ tutorial:

1. Create 2D sprites and manage their animations
2. Create a *game manager* that is able to build procedurally generated levels (enemies, food, walls, ...)
3. Handle the collisions with different types of entities + trigger the appropriate action for each one
4. Make a transition between levels
5. Create a *sound manager* in order to deal with the sound effect + the music 

If want more informations on these, feel free to read the CHANGELOGS and the comments in the code files.