﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;      //Allows us to use SceneManager
using UnityEngine.UI;


public class Player : MovingObject
{

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float delayRestartLevel = 1f;
    public Text foodText;

    // It's cleaner to make some list to ease the addition of new sound
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    // Should be in "GameManager" but as it's instatiate on the fly, we cannot assign the "gameOverClip" to it
    public AudioClip gameOverSound;


    //Used to store player food points total during level.
    private int food;
    private Animator animator;

    // Override the funtion of MovingObject
    protected override void Start()
    {
        animator = GetComponent<Animator>();
        food = GameManager.instance.playerFoodPoint;
        UpdateFoodDisplay();

        // Call the start function of the parent class
        base.Start();
    }

    private void UpdateFoodDisplay()
    {
        foodText.text = "Food: " + food;
    }

    // This function makes part of the unity api, it is called when  when the behaviour becomes disabled or inactive.
    //When Player object is disabled, store the current local food total in the GameManager
    //so it can be re-loaded in next level.
    private void OnDisable()
    {
        GameManager.instance.playerFoodPoint = food;
    }

    private void Update()
    {
        if (!GameManager.instance.playerTurn) return;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int) (Input.GetAxisRaw("Horizontal"));
        vertical = (int) (Input.GetAxisRaw("Vertical"));

        // Prevent player for moving in diagonal
        if (horizontal != 0)
            vertical = 0;

        //Call AttemptMove passing in the generic parameter Wall, since that is what Player may interact with
        //if they encounter one (by attacking it)
        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall>(horizontal, vertical);
    }

    protected override void OnCantMove<T>(T component)
    {
        Wall hitWAll = component as Wall;
        hitWAll.DamageWall(wallDamage);
        animator.SetTrigger("PlayerChop");
        SoundManager.instance.RandomizeSFX(chopSound1, chopSound2);
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        food--;
        UpdateFoodDisplay();

        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;
        if( Move(xDir, yDir, out hit))
        {
            SoundManager.instance.RandomizeSFX(moveSound1, moveSound2);
        }

        CheckIfGameOver();
        GameManager.instance.playerTurn = false;
    }

    // Handle collision with "exit" and "food"
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Exit")
        {
            // Call the "Restart" function after a delay of "delayRestartLevel"
            Invoke("Restart", delayRestartLevel);
            // the level is finished so we disable the player
            // This will call the "OnDisable" function
            enabled = false;
        }
        else if (collision.tag == "Food")
        {
            food += pointsPerFood;
            // Disable the food we collect
            collision.gameObject.SetActive(false);
            UpdateFoodDisplay();
            SoundManager.instance.RandomizeSFX(eatSound1, eatSound2);
        }
        else if (collision.tag == "Soda")
        {
            food += pointsPerSoda;
            // Disable the food we collect
            collision.gameObject.SetActive(false);
            UpdateFoodDisplay();
            SoundManager.instance.RandomizeSFX(drinkSound1, drinkSound2);
        }
    }

    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            GameManager.instance.GameOver();
            SoundManager.instance.RandomizeSFX(gameOverSound);
            SoundManager.instance.musicSource.Stop();
        }
    }

    public void LooseFood(int loss)
    {
        food -= loss;
        animator.SetTrigger("PlayerHit");
        CheckIfGameOver();
    }

    private void Restart()
    {
        // Application.LoadLevel(Application.loadedLevel); ==> obsolète
        //Load the last scene loaded, in this case Main, the only scene in the game.
        SceneManager.LoadScene(0);
    }
}
