﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
{

    public float movingTime = 0.1f;  //time between 2 move in seconds (period)
    public LayerMask blockingLayer;  //in which layers the collisions occurs

    private BoxCollider2D boxCollider;
    private Rigidbody2D rb2d;
    private float inverseMoveTime; 
    //used to make time calculation more efficient
    // it stores the fraction of move by seconds (frequency)


    // Start is called before the first frame update
    // "protected virtual" allows the function to be overriden by child
    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / movingTime;  //It's more efficient to do mulplication than dividion in a loop
    }

    // Keyword "out" means that the argument is passed by reference (alterable)
    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position; // implicit cast of Vector3 to Vector3
        Vector2 end = start + new Vector2(xDir, yDir);

        boxCollider.enabled = false;    // Avoid to hit our own collider
        hit = Physics2D.Linecast(start, end, blockingLayer);   // Check the collisions
        boxCollider.enabled = true;

        if (hit.transform == null)
        {
            // no collision detected
            StartCoroutine(smoothMovement(end));
            return true;
        }
        return false;

    }

    protected IEnumerator smoothMovement(Vector3 destination)
    {
        float sqrRemainingDistance = (transform.position - destination).sqrMagnitude;
        // We could use "magnitude" instaed of "sqrMagnitude" but the last one is more efficient 

        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rb2d.position, destination, inverseMoveTime * Time.deltaTime);
            rb2d.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - destination).sqrMagnitude;

            // Allows to wait the next frame to re-evaluate the loop condition
            yield return null;
        }
    }

    protected virtual void AttemptMove<T>(int xDir, int yDir)
        where T : Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);

        if (hit.transform != null)
        {
            // a collision occurs
            T hitComponent = hit.transform.GetComponent<T>();

            //If canMove is false and hitComponent is not equal to null, meaning MovingObject
            //is blocked and has hit something it can interact with.
            if (!canMove && hitComponent != null)
                OnCantMove(hitComponent);
        }
        // in the other cases, there is no additionnal behaviour that need to be handled
        
    } 

    // Protected functions don't need brackets if body is empty
    protected abstract void OnCantMove <T> (T component)
        where T : Component;

}
