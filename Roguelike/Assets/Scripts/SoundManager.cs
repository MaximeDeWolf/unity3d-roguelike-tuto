﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public static SoundManager instance = null;

    public AudioSource effectsSource;
    public AudioSource musicSource;

    // Default pitch is 1 so we only produce small variation in the sound
    public float lowPitchRange = 0.95f;
    public float highPitchRange = 1.05f;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        
    }

    public void PlaySingle(AudioClip clip)
    {
        effectsSource.clip = clip;
        effectsSource.Play();
    }

    // "params" keyword allows us to create a method that take an unfixed number of argument
    public void RandomizeSFX(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        // Make repetitive sounds sounds less repetitive
        effectsSource.pitch = randomPitch;
        effectsSource.clip = clips[randomIndex];
        effectsSource.Play();
    }

}
